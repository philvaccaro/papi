#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <sys/time.h> // Time
#include <math.h>

/* papi - amdpower */
#include <unistd.h>
#include <pthread.h>
#include <papi.h>
#include "amdpower_mngr.h"


/* thread variables */
long long sample = 100000;
pthread_t thread;
volatile int g_done = 0;

int retval, rv, enum_retval = 0;;
int i;
volatile int j = 0;
volatile long long start, end;
double elapsed;
double exec_time;
double energy_cpu = 0.;

volatile long long tmp;

const PAPI_component_info_t *cmpinfo = NULL;
#define MAX_POWER_EVENT 10000
unsigned long long timer[MAX_POWER_EVENT];
double read_time[MAX_POWER_EVENT];
double thread_time[MAX_POWER_EVENT];

/* name to be associated with data log files */
char kernel_name[256];

#ifdef ENABLE_AMDPOWER

#define PC_PKG_ENERGY_ID 0
#define PC_PKG_LT_LIMIT_ID 3
#define PC_PKG_LT_WINDOW_ID 4
#define PC_PKG_ST_LIMIT_ID 6
#define PC_PKG_ST_WINDOW_ID 7
#define PC_DRAM_ENERGY_ID 9
#define PC_DRAM_ST_LIMIT_ID 12

int EventSet = PAPI_NULL;

int num_cmp = 0, cmp_id = -1, amdpower_cid = -1;


#define RYZEN_EVENTS
#ifdef RYZEN_EVENTS

#define AMDPOWER_EVENT_NUM 17
int AMDPOWER_EVENT_CODES[AMDPOWER_EVENT_NUM];
const char *AMDPOWER_EVENT_NAMES[] = {"amdpower:::ENERGY:PACKAGE0",
				      "amdpower:::ENERGY:PACKAGE0CORE0",
				      "amdpower:::ENERGY:PACKAGE0CORE1",
				      "amdpower:::ENERGY:PACKAGE0CORE2",
				      "amdpower:::ENERGY:PACKAGE0CORE3",
				      "amdpower:::ENERGY:PACKAGE0CORE4",
				      "amdpower:::ENERGY:PACKAGE0CORE5",
				      "amdpower:::ENERGY:PACKAGE0CORE6",
				      "amdpower:::ENERGY:PACKAGE0CORE7",
				      "amdpower:::ENERGY:PACKAGE0CORE8",
				      "amdpower:::ENERGY:PACKAGE0CORE9",
				      "amdpower:::ENERGY:PACKAGE0CORE10",
				      "amdpower:::ENERGY:PACKAGE0CORE11",
				      "amdpower:::ENERGY:PACKAGE0CORE12",
				      "amdpower:::ENERGY:PACKAGE0CORE13",
				      "amdpower:::ENERGY:PACKAGE0CORE14",
				      "amdpower:::ENERGY:PACKAGE0CORE15"};

#define AMDPOWER_PRINT_NUM 17
const char *AMDPOWER_PRINT_NAMES[] = {"ap.pkg0.pwr",
                                      "ap.pkg0.core0.pwr",
                                      "ap.pkg0.core1.pwr",
                                      "ap.pkg0.core2.pwr",
                                      "ap.pkg0.core3.pwr",
                                      "ap.pkg0.core4.pwr",
                                      "ap.pkg0.core5.pwr",
                                      "ap.pkg0.core6.pwr",
                                      "ap.pkg0.core7.pwr",
                                      "ap.pkg0.core8.pwr",
                                      "ap.pkg0.core9.pwr",
                                      "ap.pkg0.core10.pwr",
                                      "ap.pkg0.core11.pwr",
                                      "ap.pkg0.core12.pwr",
                                      "ap.pkg0.core13.pwr",
                                      "ap.pkg0.core14.pwr",
                                      "ap.pkg0.core15.pwr"};


const char *AUX_NAMES[] = {"read.time",
                           "thread.time"};

long double AMDPOWER_EVENT_SCALES[] = {1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9,
                                       1e-9};

int AMDPOWER_EVENT_MAP[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};

#define AMDPOWER_NUM_DIFF 17 
int AMDPOWER_EVENT_DIFF[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
#endif


//long long int values[AMDPOWER_EVENT_NUM];
long long int last[AMDPOWER_EVENT_NUM];
long long int AMDPOWER_SAMPLES[MAX_POWER_EVENT][AMDPOWER_EVENT_NUM];

#endif


//----------------------------------------------------------------
void handle_error(const char *msg, int retval) {
    printf("PAPI error %d: %s in %s\n", retval, PAPI_strerror(retval), msg);
    exit(1);
}

//----------------------------------------------------------------
pthread_barrier_t g_barrier;

//----------------------------------------------------------------
void *amdpower_thread(void *arg) {


    pthread_barrier_wait(&g_barrier);

#ifdef ENABLE_AMDPOWER
    /* Read initial values into last array */
    if ((retval = PAPI_read(EventSet, last)) != PAPI_OK)
        handle_error("read0", retval);
#endif

    read_time[j]   = 0.0;
    thread_time[j] = 0.0;
    timer[j]       = 0.0;
    j++;

    start = PAPI_get_real_nsec();
    while (!g_done) {

        usleep(sample);  // 100000 == 0.1 sec

    	double thread_start = PAPI_get_real_nsec();
        //timer[j++] = (PAPI_get_real_nsec() - start);
        // get PAPI time
        timer[j] = PAPI_get_real_nsec() - start;


#ifdef ENABLE_AMDPOWER

	
    	double read_start = PAPI_get_real_nsec();
        if ((retval = PAPI_read(EventSet, AMDPOWER_SAMPLES[j])) != PAPI_OK)
                handle_error("read", retval);

            for(int iter = 0; iter < AMDPOWER_NUM_DIFF; iter++) {
                tmp = AMDPOWER_SAMPLES[j][AMDPOWER_EVENT_MAP[iter]];
                AMDPOWER_SAMPLES[j][AMDPOWER_EVENT_MAP[iter]] = AMDPOWER_SAMPLES[j][AMDPOWER_EVENT_MAP[iter]] - last[AMDPOWER_EVENT_MAP[iter]];
                last[AMDPOWER_EVENT_MAP[iter]] = tmp;
            }
	read_time[j] = (PAPI_get_real_nsec() - read_start) * 1e-9;
#endif

	thread_time[j] = PAPI_get_real_nsec() - thread_start;
        //timer[j++] = (PAPI_get_real_nsec() - start - thread_time[j]);
	
        //timer[j] = PAPI_get_real_nsec() - start;
        j++;
    }

#ifdef ENABLE_AMDPOWER
    /* Stop counting events in the Event Set */
    if ((retval = PAPI_stop(EventSet, AMDPOWER_SAMPLES[j])) != PAPI_OK)
        handle_error("stop", retval);

    if ((retval = PAPI_cleanup_eventset(EventSet)) != PAPI_OK)
        handle_error("cleanup", retval);

    if ((retval = PAPI_destroy_eventset(&EventSet)) != PAPI_OK)
        handle_error("destroy", retval);
#endif

    return NULL;
}

/* //////////////////////////////////////////////////////////////////////////// */
int amdpwr_mngr_init(long long sample_interval, char *name) {

    /* save name for logging */
    sprintf(kernel_name, "%s", name);


    sample = sample_interval;
    int retval_papi = PAPI_library_init(PAPI_VER_CURRENT);
    if (retval_papi != PAPI_VER_CURRENT) {
        fprintf(stderr, "PAPI_library_init failed\n");
        exit(1);
    }

#ifdef ENABLE_AMDPOWER
    num_cmp = PAPI_num_components();
    for (cmp_id = 0; cmp_id < num_cmp; cmp_id++) {
        if ((cmpinfo = PAPI_get_component_info(cmp_id)) == NULL) {
            fprintf(stderr, "PAPI_get_component_info failed\n");
            exit(1);
        }
        if (strstr(cmpinfo->name, "amdpower")) {
            amdpower_cid = cmp_id;
            if (cmpinfo->disabled) {
                fprintf(stderr, "No amdpower events found: %s\n", cmpinfo->disabled_reason);
                exit(1);
            }
            break;
        }
    }

    //couldn't find component
    if (cmp_id == num_cmp) {
        fprintf(stderr, "No amdpower component found\n");
        exit(1);
    }

    /* Create evset_rapl */
    PAPI_create_eventset(&EventSet);

    int tmp_code = PAPI_NATIVE_MASK;
    enum_retval = PAPI_enum_cmp_event(&tmp_code, PAPI_ENUM_FIRST, amdpower_cid);

    int e = 0;
    while (enum_retval == PAPI_OK) {

        char cmp_event[256];
        retval_papi = PAPI_event_code_to_name(tmp_code, cmp_event);

        printf("\n\nid: %d\tname: %s\n\n", e, cmp_event);
        AMDPOWER_EVENT_CODES[e] = tmp_code;
        PAPI_add_event(EventSet, AMDPOWER_EVENT_CODES[e++]);

        enum_retval = PAPI_enum_cmp_event(&tmp_code, PAPI_ENUM_EVENTS, amdpower_cid);
    }
#endif

    return retval;
}

///////////////////////////////////////////////////////////////
int amdpwr_mngr_collect() {

    int retval = 0;

#ifdef ENABLE_AMDPOWER
    /* Start counting events in the Event Set */
    if ((retval = PAPI_start(EventSet)) != PAPI_OK)
        handle_error("start", retval);
#endif

    // create thread & sync
    g_done = 0;
    //pthread_t thread;
    pthread_barrier_init(&g_barrier, NULL, 2);
    pthread_create(&thread, NULL, amdpower_thread, NULL);
    pthread_barrier_wait(&g_barrier);

    return retval;
}

///////////////////////////////////////////////////////////////
int amdpwr_mngr_stop() {

    // wait for thread to finish
    g_done = 1;
    pthread_join(thread, NULL);
    pthread_barrier_destroy(&g_barrier);

    char power_name[256];
    sprintf(power_name, "%s%s%s", "power-", kernel_name, ".csv");
    FILE *fp;
    fp = fopen(power_name, "w");

    fprintf(fp, "%10s", "timestep");

#ifdef ENABLE_AMDPOWER
    for (int ee = 0; ee < AMDPOWER_PRINT_NUM; ee++)
        fprintf(fp, ",%20s", AMDPOWER_PRINT_NAMES[ee]);
#endif

    j++; // final increment of j
    fprintf(fp, "\n");

    //double smpl_time_prev = 0.0;
    int i = 0, k = 0;
    for (i = 1; i < j - 1; i++) {

        /* first write time step */
        fprintf(fp, "%10.6Lf", (long double)timer[i]*1e-9);


        long double time_diff = (long double)(timer[i] - timer[i - 1])*1e-9;
        long double scaled_event;
#ifdef ENABLE_AMDPOWER
	double energy = 0.0f;
        double package = 0.0f;
        double core_sum = 0.0f;
        for(k = 0; k < AMDPOWER_PRINT_NUM; k++) {

            scaled_event = (long double)AMDPOWER_SAMPLES[i][AMDPOWER_EVENT_MAP[k]] * AMDPOWER_EVENT_SCALES[k];

            if(k == 0)
	    	package = scaled_event;
            else
            	core_sum += scaled_event;

            if(strstr(AMDPOWER_PRINT_NAMES[k], "pwr") != NULL) {

            	if(strstr(AMDPOWER_PRINT_NAMES[k], "pwr") != NULL) 
			energy       = scaled_event;

                scaled_event = scaled_event / time_diff;
	    }
            
            fprintf(fp,",%20.6Lf", scaled_event);
            
        }
        printf("i: %d\tpkg energy: %f\tcore sum energy: %f\n", i, package, core_sum/2.0f);
        fprintf(fp,",%20.6f", energy);
        fprintf(fp,",%20.6f", read_time[i]);
        fprintf(fp,",%20.6f", thread_time[i]*1e-9);
        fprintf(fp,",%20.6Lf", time_diff);
#endif
        fprintf(fp, "\n");
    }

    fclose(fp);
    return retval;
}

