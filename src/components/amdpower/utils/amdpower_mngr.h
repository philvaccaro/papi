#include "unistd.h"

/* initialize PAPI */
int amdpwr_mngr_init(long long sample_rate, char *name);

int amdpwr_mngr_collect();

int amdpwr_mngr_stop();

/* write frequency data to file */
void amdpwr_set_limit(long long power_limit_in);
