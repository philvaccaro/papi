#include "amdpower_mngr.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"


///////////////////////////////////////////////////////////////
static int startswith(const char *s, const char *prefix) {
    size_t n = strlen(prefix);
    if (strncmp(s, prefix, n))
        return 0;
    return 1;
}

///////////////////////////////////////////////////////////////
static void show_help(char *progname) {
    printf("%s  USAGE OPTIONS:  [--c=path_to_bin (default is sleep(5))] [ --name=name ] [ --ts=seconds (default = 0.1) ] [ --pl=watts (none by default) ] [--ls=watts] [ --r=runs (default = 1 ] ",
           progname);
    printf("  \n");
    printf("  --c=  path to binary or script\n");
    printf("  --name=  name for log files\n");
    printf("  --ts= sample time (seconds) \n");
    printf("  --pl= power limit to be applied (Watts)\n");
    printf("  --ls= for (i = 0; i < r; i++)\n\t new_pl = orig_pl + (ls * i)\n\trun(c)\n ");
    printf("  --r=  how many times to apply 'limit_step'\n");
    printf("\n\n\n");
}

///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
int main(int argc, char *argv[]) {


    long long start_limit = -1, limit_step = -1, num_limit_steps = -1;
    double sample_time = 0.1;
    char c_path[256] = {};
    char c_name[128] = {};


    for (int i = 1; i < argc && argv[i]; ++i) {

        if (startswith(argv[i], "--help")) {
            show_help(argv[0]);
            return 0;
        } else if (startswith(argv[i], "--c=")) {
            sscanf(strchr(argv[i], '=') + 1, "%s", c_path);
        } else if (startswith(argv[i], "--name=")) {
            sscanf(strchr(argv[i], '=') + 1, "%s", c_name);
        } else if (startswith(argv[i], "--ts=")) {
            sscanf(strchr(argv[i], '=') + 1, "%lf", &sample_time);
        } else if (startswith(argv[i], "--pl=")) {
            sscanf(strchr(argv[i], '=') + 1, "%lld", &start_limit);
        } else if (startswith(argv[i], "--ls=")) {
            sscanf(strchr(argv[i], '=') + 1, "%lld", &limit_step);
        } else if (startswith(argv[i], "--r=")) {
            sscanf(strchr(argv[i], '=') + 1, "%lld", &num_limit_steps);
        } else {
            fprintf(stderr, "Unknown option: %s\n", argv[i]);
            return -1;
        }
    }

    if (c_path[0] == 0) {
        printf("\nNo program specified. Using sleep 5 as default...\n");
        strcpy(c_path, "sleep 5s");
        sprintf(c_name, "%s", "sleep_5s");
    }
    else if (c_name[0] == 0) {
        char *tp;
        (tp = strrchr(c_path, '/')) ? ++tp : (tp = c_path);
        sprintf(c_name, "%s", tp);
    }


    /* handle case where you just want to set --r but not change the limit each iteration */
    if (limit_step == -1)
        limit_step = 0;

    /* handle case where you just want to set --c and run at default without recompiling */
    if (start_limit == -1)
        start_limit = 0;

    /* handle case where you just want to set --c run once without specifying --r */
    if (num_limit_steps == -1)
        num_limit_steps = 1;

    limit_step *= 10e5;
    start_limit *= 1e6;

//==============================================================================


#ifdef ENABLE_FREQ
    /* set up interface to power gadget */
    amdpwr_freq_init((long long)(1e6 * sample_time), c_name);

        /* starting reading freqeuncy */
    amdpwr_freq_read();
#endif

#if defined(ENABLE_AMDPOWER)
    /* init papi */
    amdpwr_mngr_init((long long) (1e6 * sample_time), c_name);


    /* start collecting data */
    amdpwr_mngr_collect();
#endif


    //long long curr_limit = start_limit;

    printf("sleeping for 1 seconds before starting runs...\n");
    sleep(1);
    for (int r = 0; r < num_limit_steps; r++) {

        printf("Running and logging events for %s", c_path);
#if defined(ENABLE_AMDPOWER) && defined(ENABLE_CAPPING)
        /* set the current power limit */
        amdpwr_set_limit(curr_limit + (r * limit_step));

        if(curr_limit == 0)
            printf(" at default power limit...");
        else
            printf(" at %.2f Watts...", (double) (curr_limit + (r * limit_step)) / 10e5);
#else
        printf(" at default power limit...");
#endif

        /* execute external benchmark */
        system(c_path);
        printf("done\n");
    }

#if defined(ENABLE_AMDPOWER)
    //gather PAPI power data
    amdpwr_mngr_stop();
#endif

#ifdef ENABLE_FREQ
    /* write freqeuncy data to file */
    amdpwr_freq_log();
#endif
    return 0;
}



