/**
 * @file    linux-amdpower.c
 * @author  Philip Vaccaro
 *
 * @ingroup papi_components
 *
 * @brief amdpower component
 *
 *  This component enables AMDPOWER (Running Average Power Level)
 *  energy measurements on AMD SandyBridge/IvyBridge/Haswell
 *
 *  To work, either msr_safe kernel module from LLNL
 *  (https://github.com/scalability-llnl/msr-safe), or
 *  the x86 generic MSR driver must be installed
 *    (CONFIG_X86_MSR) and the /dev/cpu/?/<msr_safe | msr> files must have read permissions
 */

#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <math.h>

/* Headers required by PAPI */
#include "papi.h"
#include "papi_internal.h"
#include "papi_vector.h"
#include "papi_memory.h"


/*
 * Platform specific AMDPOWER Domains.
 * Note that PP1 AMDPOWER Domain is supported on 062A only
 * And DRAM AMDPOWER Domain is supported on 062D only
 */


/* AMDPOWER defines */
#define MSR_AMDPOWER_POWER_UNIT             0x606

/* Package */
#define MSR_PKG_AMDPOWER_POWER_LIMIT        0x610
#define MSR_PKG_ENERGY_STATUS           0x611
#define MSR_PKG_PERF_STATUS             0x613
#define MSR_PKG_POWER_INFO              0x614

/* PP0 */
#define MSR_PP0_POWER_LIMIT             0x638
#define MSR_PP0_ENERGY_STATUS           0x639
#define MSR_PP0_POLICY                  0x63A
#define MSR_PP0_PERF_STATUS             0x63B

/* PP1 */
#define MSR_PP1_POWER_LIMIT             0x640
#define MSR_PP1_ENERGY_STATUS           0x641
#define MSR_PP1_POLICY                  0x642

/* DRAM */
#define MSR_DRAM_POWER_LIMIT            0x618
#define MSR_DRAM_ENERGY_STATUS          0x619
#define MSR_DRAM_PERF_STATUS            0x61B
#define MSR_DRAM_POWER_INFO             0x61C

/* AMDPOWER bitsmasks */
#define POWER_UNIT_OFFSET          0
#define POWER_UNIT_MASK         0x0f

#define ENERGY_UNIT_OFFSET      0x08
#define ENERGY_UNIT_MASK        0x1f

#define TIME_UNIT_OFFSET        0x10
#define TIME_UNIT_MASK          0x0f

/* AMDPOWER POWER UNIT MASKS */
#define POWER_INFO_UNIT_MASK     0x7fff
#define THERMAL_SHIFT                 0
#define MINIMUM_POWER_SHIFT          16
#define MAXIMUM_POWER_SHIFT          32
#define MAXIMUM_TIME_WINDOW_SHIFT    48


/* AMD POWER MSR MASKS */
#define MSR_POWER_LIMIT_AMD		0xC0010061
#define MSR_PSTATE_AMD			0xC0010062
#define MSR_POWER_UNIT_AMD		0xC0010299
#define MSR_CORE_ENERGY_STATUS_AMD	0xC001029A
#define MSR_PKG_ENERGY_STATUS_AMD	0xC001029B

typedef struct _amdpower_register
{
	unsigned int selector;
} _amdpower_register_t;

typedef struct _amdpower_native_event_entry
{
  char name[PAPI_MAX_STR_LEN];
  char units[PAPI_MIN_STR_LEN];
  char description[PAPI_MAX_STR_LEN];
  int fd_offset;
  off_t msr;
  int type;
  int return_type;
  _amdpower_register_t resources;
} _amdpower_native_event_entry_t;

typedef struct _amdpower_reg_alloc
{
	_amdpower_register_t ra_bits;
} _amdpower_reg_alloc_t;

/* actually 32?  But setting this to be safe? */
#define AMDPOWER_MAX_COUNTERS 64

typedef struct _amdpower_control_state
{
  int being_measured[AMDPOWER_MAX_COUNTERS];
  long long count[AMDPOWER_MAX_COUNTERS];
  int need_difference[AMDPOWER_MAX_COUNTERS];
  long long lastupdate;
} _amdpower_control_state_t;


typedef struct _amdpower_context
{
  long long start_value[AMDPOWER_MAX_COUNTERS];
  _amdpower_control_state_t state;
} _amdpower_context_t;


papi_vector_t _amdpower_vector;

struct fd_array_t {
  int fd;
  int open;
};

static _amdpower_native_event_entry_t * amdpower_native_events=NULL;
static int num_events		= 0;
struct fd_array_t *fd_array=NULL;
static int num_packages=0,num_cpus=0;

int power_divisor,time_divisor;
long long cpu_energy_divisor,dram_energy_divisor;
long long power_mult;

#define PACKAGE_ENERGY      	0
#define CORE_ENERGY      	1
#define PACKAGE_THERMAL     	2
#define PACKAGE_MINIMUM     	3
#define PACKAGE_MAXIMUM     	4
#define PACKAGE_TIME_WINDOW 	5
#define PACKAGE_ENERGY_CNT      6
#define PACKAGE_THERMAL_CNT     7
#define PACKAGE_MINIMUM_CNT     8
#define PACKAGE_MAXIMUM_CNT     9
#define PACKAGE_TIME_WINDOW_CNT 10
#define DRAM_ENERGY		11

/***************************************************************************/
/******  BEGIN FUNCTIONS  USED INTERNALLY SPECIFIC TO THIS COMPONENT *******/
/***************************************************************************/


static long long read_msr(int fd, off_t which) {

  uint64_t data;

  if ( fd<0 || pread(fd, &data, sizeof data, which) != sizeof data ) {
    perror("rdmsr:pread");
    exit(127);
  }

  return (long long)data;
}

static int open_fd(int offset) {
  
  int fd=-1;
  char filename[BUFSIZ];

  if (fd_array[offset].open==0) {
	  sprintf(filename,"/dev/cpu/%d/msr_safe",offset);
      fd = open(filename, O_RDONLY);
	  if (fd<0) {
		  sprintf(filename,"/dev/cpu/%d/msr",offset);
          fd = open(filename, O_RDONLY);
	  }
	  if (fd>=0) {
		  fd_array[offset].fd=fd;
	      fd_array[offset].open=1;
      } 
  }
  else {
    fd=fd_array[offset].fd;
  }

  return fd;
}

static long long read_amdpower_value(int index) {

   int fd;

   fd=open_fd(amdpower_native_events[index].fd_offset);
   return read_msr(fd,amdpower_native_events[index].msr);

}

static long long convert_amdpower_energy(int index, long long value) {

   long long ret = value;
   if (amdpower_native_events[index].type==PACKAGE_ENERGY) {
      ret = (long long)(((double)value / cpu_energy_divisor) * 1e9);
   }
   if (amdpower_native_events[index].type==CORE_ENERGY) {
      ret = (long long)(((double)value / cpu_energy_divisor) * 1e9);
   }

   return ret;
}

static int
get_kernel_nr_cpus(void)
{
  FILE *fff;
  int num_read, nr_cpus = 1;
  fff=fopen("/sys/devices/system/cpu/kernel_max","r");
  if (fff==NULL) return nr_cpus;
  num_read=fscanf(fff,"%d",&nr_cpus);
  fclose(fff);
  if (num_read==1) {
    nr_cpus++;
  } else {
    nr_cpus = 1;
  }
  return nr_cpus;
}

/************************* PAPI Functions **********************************/


/*
 * This is called whenever a thread is initialized
 */
static int
_amdpower_init_thread( hwd_context_t *ctx )
{
  ( void ) ctx;

  return PAPI_OK;
}



/*
 * Called when PAPI process is initialized (i.e. PAPI_library_init)
 */
static int
_amdpower_init_component( int cidx )
{
     int i,j,k,fd;
     FILE *fff;
     char filename[BUFSIZ];

     int package_avail, dram_avail, pp0_avail, pp1_avail;

     int num_packages, num_cores_per_package, num_threads_per_core;

     //long long result;
     uint64_t result;
     int package;

     const PAPI_hw_info_t *hw_info;

     int nr_cpus = get_kernel_nr_cpus();
     int packages[nr_cpus];
     int cpu_to_use[nr_cpus];

     /* Fill with sentinel values */
     for (i=0; i<nr_cpus; ++i) {
       packages[i] = -1;
       cpu_to_use[i] = -1;
     }


     /* check if AMD processor */
     hw_info=&(_papi_hwi_system_info.hw_info);


     /* Ugh can't use PAPI_get_hardware_info() if
	PAPI library not done initializing yet */

     if (hw_info->vendor!=PAPI_VENDOR_AMD) {
        strncpy(_amdpower_vector.cmp_info.disabled_reason,
		"Not an AMD processor",PAPI_MAX_STR_LEN);
        return PAPI_ENOSUPP;
     }

     /* check model to support */
     if (hw_info->cpuid_family==23) {
       if (hw_info->cpuid_model==1) {
	  /* AMD Ryzen */
       }
       else {
	 /* not a supported model */
	 strncpy(_amdpower_vector.cmp_info.disabled_reason,
		 "CPU model not supported",
		 PAPI_MAX_STR_LEN);
	 return PAPI_ENOIMPL;
       }
     }
     else {
       /* Not a family 17> machine */
       strncpy(_amdpower_vector.cmp_info.disabled_reason,
	       "CPU family not supported",PAPI_MAX_STR_LEN);
       return PAPI_ENOIMPL;
     }

     /* get topology for event creation */
     num_packages          = hw_info->sockets;
     num_cores_per_package = hw_info->cores;
     num_threads_per_core  = hw_info->threads;
     num_cpus              = num_packages * num_cores_per_package * num_threads_per_core;
     
     /* Init fd_array */
     fd_array=papi_calloc(sizeof(struct fd_array_t),num_cpus);
     if (fd_array==NULL) return PAPI_ENOMEM;

     fd=open_fd(0);
     if (fd<0) {
        sprintf(_amdpower_vector.cmp_info.disabled_reason,
     		"Can't open fd for cpu0: %s",strerror(errno));
        return PAPI_ESYS;
     }

     /* get energy divisor */
     result=read_msr(fd,MSR_POWER_UNIT_AMD);
     cpu_energy_divisor = (1 << ((result >> 8) & 0x1F));
     power_mult         = 1e6 / (1 << (result & 0xF));
     long long time_mult         = (result>>16) & 0xF;
     	
     fd=open_fd(0);
     result=read_msr(fd,MSR_POWER_LIMIT_AMD);
/*
     int c = 0;
     for(c = 0; c < num_cpus; c++) {
     	fd=open_fd(c);
     	printf("\nCPU: %d\n", c);
     	result=read_msr(fd,MSR_CORE_ENERGY_STATUS_AMD);
     	double ergy = (double)(result * cpu_energy_divisor) * 1e-9;
     	printf("read energy: %lf J\n", ergy);
     	printf("usleep(500000)\n");
     	usleep(500000);
     	result=read_msr(fd,MSR_CORE_ENERGY_STATUS_AMD);
     	double ergy1 = (double)(result * cpu_energy_divisor) * 1e-9;
     	printf("read energy: %lf J\n", ergy1);
     	printf("energy consumed during usleep(500000) = %lf J\n",ergy1 - ergy);
     }
*/

     num_events = num_packages + (num_packages * num_cpus);
     /* Allocate space for events */
     /* Include room for both counts and scaled values */
     amdpower_native_events = (_amdpower_native_event_entry_t*)
          papi_calloc(sizeof(_amdpower_native_event_entry_t),num_events);


     for(j = 0; j < num_packages; j++) {
          /* add energy event(s) for each package */
          int first_cpu = j + (j * num_cores_per_package);
          int event_idx = j * num_cores_per_package;
     	  sprintf(amdpower_native_events[event_idx].name, "ENERGY:PACKAGE%d", j);
	  sprintf(amdpower_native_events[event_idx].description,
		   "Energy; package %d",j);
	  amdpower_native_events[event_idx].fd_offset          = first_cpu;
	  amdpower_native_events[event_idx].msr                = MSR_PKG_ENERGY_STATUS_AMD;
	  amdpower_native_events[event_idx].resources.selector = event_idx;
	  amdpower_native_events[event_idx].type               = PACKAGE_ENERGY;
	  amdpower_native_events[event_idx].return_type        = PAPI_DATATYPE_UINT64;
          for(k = 0; k < num_cpus; k++) {
               /* add event(s) for each cpu in each package energy */
               int cpu_id = k + (j * num_cores_per_package);
               event_idx  = cpu_id + 1;
     	       sprintf(amdpower_native_events[event_idx].name, "ENERGY:PACKAGE%dCORE%d", j, k);
	       sprintf(amdpower_native_events[event_idx].description,
		   "Energy; package %d core %d",j, k);
	       amdpower_native_events[event_idx].fd_offset          = cpu_id;
	       amdpower_native_events[event_idx].msr                = MSR_CORE_ENERGY_STATUS_AMD;
	       amdpower_native_events[event_idx].resources.selector = event_idx;
	       amdpower_native_events[event_idx].type               = CORE_ENERGY;
	       amdpower_native_events[event_idx].return_type        = PAPI_DATATYPE_UINT64;
          }
     }

     /* Export the total number of events available */
     _amdpower_vector.cmp_info.num_native_events = num_events;
     _amdpower_vector.cmp_info.num_cntrs         = num_events;
     _amdpower_vector.cmp_info.num_mpx_cntrs     = num_events;


     /* Export the component id */
     _amdpower_vector.cmp_info.CmpIdx = cidx;

     return PAPI_OK;
}


/*
 * Control of counters (Reading/Writing/Starting/Stopping/Setup)
 * functions
 */
static int 
_amdpower_init_control_state( hwd_control_state_t *ctl)
{

  _amdpower_control_state_t* control = (_amdpower_control_state_t*) ctl;
  int i;
  
  for(i=0;i<AMDPOWER_MAX_COUNTERS;i++) {
     control->being_measured[i]=0;
  }

  return PAPI_OK;
}

static int 
_amdpower_start( hwd_context_t *ctx, hwd_control_state_t *ctl)
{
  _amdpower_context_t* context = (_amdpower_context_t*) ctx;
  _amdpower_control_state_t* control = (_amdpower_control_state_t*) ctl;
  long long now = PAPI_get_real_usec();
  int i;

  for( i = 0; i < AMDPOWER_MAX_COUNTERS; i++ ) {
     if ((control->being_measured[i]) && (control->need_difference[i])) {
        context->start_value[i]=read_amdpower_value(i);
     }
  }

  control->lastupdate = now;

  return PAPI_OK;
}

static int 
_amdpower_stop( hwd_context_t *ctx, hwd_control_state_t *ctl )
{

    /* read values */
    _amdpower_context_t* context = (_amdpower_context_t*) ctx;
    _amdpower_control_state_t* control = (_amdpower_control_state_t*) ctl;
    long long now = PAPI_get_real_usec();
    int i;
    long long temp;

    for ( i = 0; i < AMDPOWER_MAX_COUNTERS; i++ ) {
		if (control->being_measured[i]) {
			temp = read_amdpower_value(i);
			if (context->start_value[i])
			if (control->need_difference[i]) {
				if (temp < context->start_value[i] ) {
	        		SUBDBG("Wraparound!\nstart:\t%#016x\ttemp:\t%#016x",
	        			(unsigned)context->start_value[i], (unsigned)temp);
	           		temp += (0x100000000 - context->start_value[i]);
	           		SUBDBG("\tresult:\t%#016x\n", (unsigned)temp);
		  		} else {
					temp -= context->start_value[i];
		  		}
			}
			control->count[i] = convert_amdpower_energy( i, temp );
		}
    }
    control->lastupdate = now;
    return PAPI_OK;
}

/* Shutdown a thread */
static int
_amdpower_shutdown_thread( hwd_context_t *ctx )
{
  ( void ) ctx;
  return PAPI_OK;
}

int 
_amdpower_read( hwd_context_t *ctx, hwd_control_state_t *ctl,
	    long long **events, int flags)
{
    (void) flags;

    _amdpower_stop( ctx, ctl );

    /* Pass back a pointer to our results */
    *events = ((_amdpower_control_state_t*) ctl)->count;

    return PAPI_OK;
}


/*
 * Clean up what was setup in  amdpower_init_component().
 */
static int 
_amdpower_shutdown_component( void ) 
{
    int i;

    if (amdpower_native_events) papi_free(amdpower_native_events);
    if (fd_array) {
       for(i=0;i<num_cpus;i++) {
	  if (fd_array[i].open) close(fd_array[i].fd);
       }
       papi_free(fd_array);
    }

    return PAPI_OK;
}


/* This function sets various options in the component
 * The valid codes being passed in are PAPI_SET_DEFDOM,
 * PAPI_SET_DOMAIN, PAPI_SETDEFGRN, PAPI_SET_GRANUL * and PAPI_SET_INHERIT
 */
static int
_amdpower_ctl( hwd_context_t *ctx, int code, _papi_int_option_t *option )
{
    ( void ) ctx;
    ( void ) code;
    ( void ) option;

    return PAPI_OK;
}


static int
_amdpower_update_control_state( hwd_control_state_t *ctl,
			    NativeInfo_t *native, int count,
			    hwd_context_t *ctx )
{
  int i, index;
    ( void ) ctx;

    _amdpower_control_state_t* control = (_amdpower_control_state_t*) ctl;

    /* Ugh, what is this native[] stuff all about ?*/
    /* Mostly remap stuff in papi_internal */

    for(i=0;i<AMDPOWER_MAX_COUNTERS;i++) {
       control->being_measured[i]=0;
    }
    for( i = 0; i < count; i++ ) {
       index=native[i].ni_event&PAPI_NATIVE_AND_MASK;
       native[i].ni_position=amdpower_native_events[index].resources.selector;
       control->being_measured[index]=1;
    }

    return PAPI_OK;
}


/*
 * This function has to set the bits needed to count different domains
 * In particular: PAPI_DOM_USER, PAPI_DOM_KERNEL PAPI_DOM_OTHER
 * By default return PAPI_EINVAL if none of those are specified
 * and PAPI_OK with success
 * PAPI_DOM_USER is only user context is counted
 * PAPI_DOM_KERNEL is only the Kernel/OS context is counted
 * PAPI_DOM_OTHER  is Exception/transient mode (like user TLB misses)
 * PAPI_DOM_ALL   is all of the domains
 */
static int
_amdpower_set_domain( hwd_control_state_t *ctl, int domain )
{
    ( void ) ctl;
    
    /* In theory we only support system-wide mode */
    /* How to best handle that? */
    if ( PAPI_DOM_ALL != domain )
	return PAPI_EINVAL;

    return PAPI_OK;
}


static int
_amdpower_reset( hwd_context_t *ctx, hwd_control_state_t *ctl )
{
    ( void ) ctx;
    ( void ) ctl;
	
    return PAPI_OK;
}


/*
 * Native Event functions
 */
static int
_amdpower_ntv_enum_events( unsigned int *EventCode, int modifier )
{

     int index;

     switch ( modifier ) {

	case PAPI_ENUM_FIRST:

	   if (num_events==0) {
	      return PAPI_ENOEVNT;
	   }
	   *EventCode = 0;

	   return PAPI_OK;
		

	case PAPI_ENUM_EVENTS:
	
	   index = *EventCode & PAPI_NATIVE_AND_MASK;

	   if ( index < num_events - 1 ) {
	      *EventCode = *EventCode + 1;
	      return PAPI_OK;
	   } else {
	      return PAPI_ENOEVNT;
	   }
	   break;
	
	default:
		return PAPI_EINVAL;
	}

	return PAPI_EINVAL;
}

/*
 *
 */
static int
_amdpower_ntv_code_to_name( unsigned int EventCode, char *name, int len )
{

     int index = EventCode & PAPI_NATIVE_AND_MASK;

     if ( index >= 0 && index < num_events ) {
	strncpy( name, amdpower_native_events[index].name, len );
	return PAPI_OK;
     }

     return PAPI_ENOEVNT;
}

/*
 *
 */
static int
_amdpower_ntv_code_to_descr( unsigned int EventCode, char *name, int len )
{
     int index = EventCode;

     if ( index >= 0 && index < num_events ) {
	strncpy( name, amdpower_native_events[index].description, len );
	return PAPI_OK;
     }
     return PAPI_ENOEVNT;
}

static int
_amdpower_ntv_code_to_info(unsigned int EventCode, PAPI_event_info_t *info) 
{

  int index = EventCode;

  if ( ( index < 0) || (index >= num_events )) return PAPI_ENOEVNT;

  strncpy( info->symbol, amdpower_native_events[index].name, sizeof(info->symbol)-1);
  info->symbol[sizeof(info->symbol)-1] = '\0';

  strncpy( info->long_descr, amdpower_native_events[index].description, sizeof(info->long_descr)-1);
  info->long_descr[sizeof(info->long_descr)-1] = '\0';

  strncpy( info->units, amdpower_native_events[index].units, sizeof(info->units)-1);
  info->units[sizeof(info->units)-1] = '\0';

  info->data_type = amdpower_native_events[index].return_type;

  return PAPI_OK;
}



papi_vector_t _amdpower_vector = {
    .cmp_info = { /* (unspecified values are initialized to 0) */
       .name = "amdpower",
       .short_name = "amdpower",
       .description = "Linux AMDPOWER energy measurements",
       .version = "5.3.0",
       .default_domain = PAPI_DOM_ALL,
       .default_granularity = PAPI_GRN_SYS,
       .available_granularities = PAPI_GRN_SYS,
       .hardware_intr_sig = PAPI_INT_SIGNAL,
       .available_domains = PAPI_DOM_ALL,
    },

	/* sizes of framework-opaque component-private structures */
    .size = {
	.context = sizeof ( _amdpower_context_t ),
	.control_state = sizeof ( _amdpower_control_state_t ),
	.reg_value = sizeof ( _amdpower_register_t ),
	.reg_alloc = sizeof ( _amdpower_reg_alloc_t ),
    },
	/* function pointers in this component */
    .init_thread =          _amdpower_init_thread,
    .init_component =       _amdpower_init_component,
    .init_control_state =   _amdpower_init_control_state,
    .start =                _amdpower_start,
    .stop =                 _amdpower_stop,
    .read =                 _amdpower_read,
    .shutdown_thread =      _amdpower_shutdown_thread,
    .shutdown_component =   _amdpower_shutdown_component,
    .ctl =                  _amdpower_ctl,

    .update_control_state = _amdpower_update_control_state,
    .set_domain =           _amdpower_set_domain,
    .reset =                _amdpower_reset,

    .ntv_enum_events =      _amdpower_ntv_enum_events,
    .ntv_code_to_name =     _amdpower_ntv_code_to_name,
    .ntv_code_to_descr =    _amdpower_ntv_code_to_descr,
    .ntv_code_to_info =     _amdpower_ntv_code_to_info,
};
